# Impressum, Disclaimer, AGB

<sup>_(Diese Aufgabe ist für eine TEAMS-Aufgabe ausgelegt)_</sup>

Kompetenz und Lernziel siehe [Modulbaukasten](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden) (Hz 5.2)

Zeitbedarf ca. 60-90 min

Erstellen Sie eine eigene Seite in Ihrem Dossier für diese Aufgabe und **geben Sie am Schluss den Link auf diese Seite** ab.

Es gibt wesentliche internationale Vorschriften für die juristischen Voraussetzungen für Internetseiten. Es sind dies das **Impressum**, der **Disclaimer** und die **AGB**. Hier in dieser Aufgabe geht es darum, dass Sie erklären, was diese Begriffe bedeuten und wann und wo sie angewendentet werden müssen, aber auch, wo sie weggelassen werden dürfen.


Suchen Sie im Internet nach den entsprechenden Antworten.

- Aufgabe 1: Übersetzen Sie "Impressum" auf folgende Sprachen
	- Deutsch (ein Synonym oder eine Alternative)
	- Englisch
	- Französisch
	- Italienisch
	- Spanisch
	- und in Ihre Herkunftssprache, wenn Sie zu Hause nicht eine der obigen Sprachen sprechen

- Aufgabe 2: Was bedeutet ein "Impressum"?

- Aufgabe 3: Wann besteht in der Schweiz Impressum-Pflicht? Und wann nicht?

- Aufgabe 4: Wann besteht in Deutschland (oder in der EU) Impressum-Pflicht? Und wann nicht? Gibt es Unterschiede zur Schweiz?

- Aufgabe 5: Übersetzen Sie "Disclaimer" auf folgende Sprachen
	- Deutsch
	- Englisch (ein Synonym oder eine Alternative)
	- Französisch
	- Italienisch
	- Spanisch
	- und in Ihre Herkunftssprache, wenn Sie zu Hause nicht eine der obigen Sprachen sprechen

- Aufgabe 6: Was bedeutet / was ist ein "Disclaimer" und warum macht man sowas auf eine Internetauftritt (Homepage, Website)?

- Aufgabe 7: Was bedeuten die drei Buchstaben "AGB" und was steht da so in Etwa drin? Machen Sie in 3-4 ausgeschriebenen Sätzen eine kleine selbstgeschriebene Zusammenfassung. 

- Aufgabe 8: Zeigen Sie 3 verschiedene Links auf AGB's von Firmen oder auch Vorlagen von AGBs für den eigenen Gebrauch. (bei Abgabe gleicher Links wie Ihre Schulkolleg:innen wird die Aufgabe zur Überarbeitung zurückgegeben)
